// console.log("Hello, Hi!");

// s20 Review
/*
for(let number = 0; number < 10; number++){
    
    if(number == 2){
        console.log("Number 2 is skip next line of codes then continue the loop");
        continue;
    }
    
    if(number != 2){
        console.log(number);
    }
    
    if(number == 5){
    	console.log("Stop the loop at number 5");
        break;
    }
}*/
/*
1. Initialize a value
2. Checks the condition
3. Run the statements inside the loop
4. Change of value (increment or decrement)
	-Go back to step 2
*/

// Reassigning with Concatenation of String
/*let myString = "doooom";
let letterO = "";

for (let i=0; i<myString.length; i++){
	if(myString[i] == 'o'){
		letterO = letterO + myString[i];
	}
}
console.log(letterO);*/

// Array
// An array in programming is simply a list of data. Let's write example erlier

// A
let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// B
let studentNumbers = ['2020-1923','2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// Common examples of arrays

// Same data type elements of an array
// Index       0     1     2    3
let grades = [98.5, 94.3, 89.2, 90.1, 99];
//                      0       1       2       3
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use if an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative ways to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake bootstrap'
];

console.log(myTasks);

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// [SECTION] length property
// The .length property allows us to get and set the total numbers of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// Length property can also be used with strings. Some array methods and properties can also be used with strings

let fullName = "Jamie Oliver";
console.log(fullName.length);

// Removing an element in an array

/*let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake bootstrap'
];

console.log(myTasks);
*/

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities.length);
console.log(cities);


// however, we can't do the same on strings
console.log("Initial length of fullName " + fullName.length);
fullName.length = fullName.length-1;
console.log("current length of fullname: " + fullName.length);
console.log(fullName);

/*
let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles++;
console.log(theBeatles);
*/

// Accessing th element of an array through index
// let grades = [98.5, 94.3, 89.2, 90.1, 99];
console.log(grades[0]);

// let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
console.log(computerBrands[3]);

// let grades = [98.5, 94.3, 89.2, 90.1, 99];
function getGrade(index){
    console.log(grades[index]);

}
getGrade(3);
getGrade(grades.length-1);

let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// Change an element/ Re-assigning array values
console.log('Array before re-assignment');
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegend);

// Change the last element
let bullsLegend = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullsLegend.length-1;
// console.log(lastElementIndex);
console.log(bullsLegend[lastElementIndex]);
// you could also access it directly
console.log(bullsLegend[bullsLegend.length-1]);
bullsLegend[lastElementIndex] = "Harper";
console.log(bullsLegend);

// Add items into the array
// Adding elements after the last element

let newArr = [];
console.log(newArr[0]);

newArr[0]= "Cloud Strife";
console.log(newArr);

newArr[2]= "Tifa Lockhart";
console.log(newArr);

// Current output: ['Cloud Strife', empty, 'Tifa Lockhart']
// Adding elements afer the last element
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over and array (display per elements of an array)
for(let index=0; index<newArr.length; index++){
    console.log(newArr[index]); // 0 // 1 //
}

let numArr = [5, 12, 30, 46, 40];

// a loop that will check per element if divisible by 5
for(let index=0; index<numArr.length; index++){
    if(numArr[index] % 5 === 0){
        console.log(numArr[index] + " is divisible by 5");
    }
    else{
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// [SECTION] Multidimensional Arrays
//Arrays inside an array
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'h8'],
    ];
console.log(chessBoard);
console.log(chessBoard[1][4]);
                        //row //col

// Mini activity - display c5
console.log(chessBoard[4][2]);
console.log("Pawn moves to: " + chessBoard[1][5]);
